from pymongo import MongoClient
from models.books import database as db
import csv
from bson.objectid import ObjectId

def add_book():
    data = open("bestsellers-with-categories.csv",encoding='utf-8')
    books = csv.reader(data, delimiter=',')
    next(books)

    for book in books:
        try:
            data = {
                "nama": book[0],
                "pengarang": book[1],
                "tahun terbit": book[5],
                "genre": book[6]
            }
            db.insertBook(data)
        except Exception as e:
            print("Kesalahan pada saat memasukan data: {}".format(e))
            break

def search_books(params):
    for book in db.searchBookByName(params):
        print(book)

def show_all():
    for book in db.showBooks():
        print(book)

def show_books_id(params):
    for book in db.showBookById(params):
        print(book)
    
def deletebook(params):
    db.deleteBookById(params)

if __name__ == "__main__":
    db = db()
    #kata_kunci = "harry"
    #earch_books(kata_kunci)
    #add_book()
    iud=ObjectId("606d31a0ab921732391d46cc")
    show_books_id(iud)
    #show_all()
    #deletebook(iud)
    db.nosql_db.close()